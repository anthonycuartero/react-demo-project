import React, { Component } from 'react';
import './App.css';
import axios from 'axios';

import GoogleLogin from 'react-google-login';
import FacebookLogin from 'react-facebook-login';

const formStyle = {
    margin: 'auto',
    padding: '10px',
    border: '1px solid #c9c9c9',
    borderRadius: '5px',
    background: '#f5f5f5',
    width: '220px',
  	display: 'block'
};

const inputStyle = {
    margin: '5px 0 10px 0',
    padding: '5px', 
    border: '1px solid #bfbfbf',
    borderRadius: '3px',
    boxSizing: 'border-box',
    width: '100%'
};

const submitStyle = {
    margin: '10px 0 0 0',
    padding: '7px 10px',
    border: '1px solid #efffff',
    borderRadius: '3px',
    background: '#3085d6',
    width: '100%', 
    fontSize: '15px',
    color: 'white',
    display: 'block',
    cursor: 'pointer',
};

const api = axios.create();

class App extends Component{
  constructor(){
    super();
    this.state = {
      username: '',
      password: '',
      result: '',
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  }

  loginUser = async () => {
    var data = {
      email: this.state.username,
      password: this.state.password,
    }
    let res = await api.post('https://localhost:44385/api/demo/signin', data, {
      headers: {
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*"
      }
    });
    localStorage.setItem('token', res.data.access_token);
    alert("Successfully login");
    console.log(res.data);
  }

  fetchApi = async () => {
    var accessToken = localStorage.getItem('token');
    let res = await api.get('https://localhost:44385/api/demo', {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;',
        "Access-Control-Allow-Origin": "*",
        'Authorization': 'Bearer ' + accessToken
      }
    });
    console.log(res);
    this.setState({ result: JSON.stringify(res) });
  }

  responseFacebook = async(response) => {
    var data = {
      id: response.id,
      email: response.email,
    }
    let res = await api.post('https://localhost:44385/api/demo/externallogin', data, {
      headers: {
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*"
      }
    });
    
    localStorage.setItem('token', res.data.access_token);
    alert("Successfully login");
  }

  
  responseGoogle = async(response) => {
    var data = {
      id: response.profileObj.googleId,
      email: response.profileObj.email,
    }
    let res = await api.post('https://localhost:44385/api/demo/externallogin', data, {
      headers: {
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*"
      }
    });
    localStorage.setItem('token', res.data.access_token);
    alert("Successfully login");
  }

  render(){
    return (
      <div className="App">
        <header className="App-header">
  
        <form style={formStyle} >
        <input
        style={inputStyle}
                      placeholder="Email"
                      type="text"
                      name="username"
                      value={this.state.username}
                      autoComplete="new-email"
                      onChange={this.handleChange}
                    />
          <input
          style={inputStyle}
                      placeholder="Password"
                      type="password"
                      value={this.state.password}
                      name="password"
                      autoComplete="new-password"
                      onChange={this.handleChange}
                    />
          <div>
            <button style={submitStyle} type="button" onClick={this.loginUser.bind(this)}>Submit</button>
          </div>
          <button style={submitStyle} type="button" onClick={this.fetchApi.bind(this)}>Fetch API</button>

          <FacebookLogin
          style={submitStyle}
            appId="220522130038243"
            fields="name,email,picture"
            icon="fa-facebook"
            callback={this.responseFacebook}
          />

        <GoogleLogin
            clientId="1025165228502-6trsivqntuknv33qeop9k209q1n32n31.apps.googleusercontent.com"
            buttonText="Google"
            onSuccess={this.responseGoogle}
          />

        </form>
        <p>Api result: {this.state.result} </p>
        </header>
      </div>
    );
  }
}
export default App;
